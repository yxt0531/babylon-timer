# Babylon Timer

A pomodoro timer written in Babylon.js.

*This is mostly a study project of using Babylon.js and electron-webpack to build desktop games, it is impractical to use it as an actual timer due to its size and high resource usage. The source code is available on [GitLab](https://gitlab.com/yxt0531/babylon-timer), the dessert city scene used in this app is made by lscarci@icloud.com.*

*All licenses related to this project is under the licenses folder on [GitLab](https://gitlab.com/yxt0531/babylon-timer/tree/master/licenses) repository.*
