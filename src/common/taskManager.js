var taskList = [];
var currentTask = null;
var originalMin;
var originalShort;
var currentStatus; // 0 = standby, 1 = work, 2 = short break, 3 = long break, -1 = paused

var minuteBuffer = 0;

const isDevelopment = process.env.NODE_ENV !== 'production'
var beep;

if (isDevelopment) {
    beep = new Audio("/beep.wav");
} else {
    beep = new Audio(__static + "/beep.wav");
}

export var addTask = function (n, m, s, sh, lo) {
    var task = {
        name: n,
        minute: m,
        step: s,
        shortBreak: sh,
        longBreak: lo
    };
    taskList.push(task);
};

export var getTaskList = function () {
    return taskList;
};

export var removeTask = function (index) {
    taskList.splice(index);
};

export var clearTask = function () {
    taskList = [];
    currentTask = null;
    currentStatus = 0;
};

export var startTask = function () {
    if (Array.isArray(taskList) && taskList.length) {
        currentTask = taskList.shift();
        // convert minutes in current task to seconds
        currentTask.minute = currentTask.minute * 60;
        currentTask.shortBreak = currentTask.shortBreak * 60;
        currentTask.longBreak = currentTask.longBreak * 60;

        originalMin = currentTask.minute;
        originalShort = currentTask.shortBreak;
    }

};

export var runLoop = function (delta) {
    if (currentTask != null && currentStatus != -1) {
        if (minuteBuffer < 1000) {
            minuteBuffer = minuteBuffer + delta;
        }
        else if (minuteBuffer >= 1000) {
            // run following every second

            minuteBuffer = minuteBuffer / 1000;

            if (currentTask.minute >= 0) {
                currentTask.minute = currentTask.minute - minuteBuffer;
                currentStatus = 1;
                // console.log("Current Task Minute: " + currentTask.minute)
            } else if (currentTask.shortBreak >= 0) {
                currentTask.shortBreak = currentTask.shortBreak - minuteBuffer;
                currentStatus = 2;
                // console.log("Current Task Short Break: " + currentTask.shortBreak)
            } else if (currentTask.step > 1) {
                currentTask.step--;
                currentTask.minute = originalMin;
                currentTask.shortBreak = originalShort;
                // console.log("Current Task Step: " + currentTask.step)
            } else if (currentTask.step == 1) {
                if (currentTask.longBreak >= 0) {
                    currentTask.longBreak = currentTask.longBreak - minuteBuffer;
                    currentStatus = 3;
                    // console.log("Current Task Long Break: " + currentTask.longBreak)
                } else {
                    currentTask = null; // task finished
                    currentStatus = 0;
                    // console.log("Current Task Finished.")
                }
            }

            minuteBuffer = 0;
        }
    }
};
var previousStatus = currentStatus;

export var getCurrentTaskStatus = function() {
    if (previousStatus != currentStatus) {
        // status changed
        beep.play();
    }
    if (currentStatus == 0) {
        previousStatus = currentStatus;
        return "Standby";
    } else if (currentStatus == 1) {
        previousStatus = currentStatus;
        return currentTask.name + ": " + currentTask.minute.toFixed(0) + " seconds remaining...";
    } else if (currentStatus == 2) {
        previousStatus = currentStatus;
        return "Short break, " + currentTask.shortBreak.toFixed(0) + " seconds and " + (currentTask.step - 1).toFixed(0) + " step remaining..." ;
    } else if (currentStatus == 3) {
        previousStatus = currentStatus;
        return "Task finished, " + currentTask.longBreak.toFixed(0) + " second until next task.";
    } else if (currentStatus == -1) {
        previousStatus = currentStatus;
        return "Paused";
    } else {
        previousStatus = currentStatus;
        return "Standby";
    }
};

export var pauseCurrentTask = function() {
    if (currentStatus == -1) { 
        currentStatus = 0;
    } else {
        currentStatus = -1;
    }
}