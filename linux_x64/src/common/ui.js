import * as GUI from "babylonjs-gui";
import * as TASK_MANAGER from "common/taskManager"

export var fpsLabel = new GUI.TextBlock(); // label used to display current fps
fpsLabel.text = "N/A";
fpsLabel.color = "white";
fpsLabel.fontSizeInPixels = 16;
fpsLabel.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
fpsLabel.textVerticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
fpsLabel.paddingLeftInPixels = 2;
fpsLabel.paddingTopInPixels = 2;

// begin of new task screen
export var newTaskScreen = new GUI.Grid(); // grid as new task screen contrainer
newTaskScreen.width = "50%";
newTaskScreen.heightInPixels = 400;
newTaskScreen.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
newTaskScreen.paddingLeft = "10%";
newTaskScreen.addColumnDefinition(1);
newTaskScreen.addRowDefinition(0.125);
newTaskScreen.addRowDefinition(0.125);
newTaskScreen.addRowDefinition(0.75);

var labelTask = new GUI.TextBlock(); // static label
labelTask.text = "Task:";
labelTask.color = "white";
labelTask.fontSizeInPixels = 24;
newTaskScreen.addControl(labelTask, 0, 0);

var inputTextTask = new GUI.InputText(); // textbox input for task name
inputTextTask.text = "Unnamed Task";
inputTextTask.fontSizeInPixels = 24;
inputTextTask.widthInPixels = 360;
inputTextTask.heightInPixels = 28;
inputTextTask.color = "white";
newTaskScreen.addControl(inputTextTask, 1, 0);

var newTaskChildGrid = new GUI.Grid(); // grid as new task screen sub contrainer
newTaskChildGrid.addColumnDefinition(0.5);
newTaskChildGrid.addColumnDefinition(0.5);
newTaskChildGrid.addRowDefinition(1 / 6);
newTaskChildGrid.addRowDefinition(1 / 6);
newTaskChildGrid.addRowDefinition(1 / 6);
newTaskChildGrid.addRowDefinition(1 / 6);
newTaskChildGrid.addRowDefinition(1 / 6);
newTaskChildGrid.addRowDefinition(1 / 6);
newTaskScreen.addControl(newTaskChildGrid, 2, 0);

var labelMinute = new GUI.TextBlock(); // static label
labelMinute.text = "Minute:";
labelMinute.color = "white";
labelMinute.fontSizeInPixels = 24;
newTaskChildGrid.addControl(labelMinute, 0, 0);

var inputTextMinute = new GUI.InputText(); // textbox input for minute
inputTextMinute.text = "25";
inputTextMinute.fontSizeInPixels = 24;
inputTextMinute.widthInPixels = 110;
inputTextMinute.heightInPixels = 28;
inputTextMinute.color = "white";
inputTextMinute.onTextChangedObservable.add(function () {
    // input validation
    if (isNaN(inputTextMinute.text.valueOf())) {
        inputTextMinute.text = "25";
    } else if (inputTextMinute.text.valueOf() < 1) {
        inputTextMinute.text = "1";
    } else if (inputTextMinute.text.valueOf() > 999) {
        inputTextMinute.text = "999";
    }
    updateTotalTimeCount();
});
newTaskChildGrid.addControl(inputTextMinute, 1, 0);

var labelStep = new GUI.TextBlock(); // static label
labelStep.text = "Step:";
labelStep.color = "white";
labelStep.fontSizeInPixels = 24;
newTaskChildGrid.addControl(labelStep, 0, 1);

var inputTextStep = new GUI.InputText(); // textbox input for step
inputTextStep.text = "4";
inputTextStep.fontSizeInPixels = 24;
inputTextStep.widthInPixels = 110;
inputTextStep.heightInPixels = 28;
inputTextStep.color = "white";
inputTextStep.onTextChangedObservable.add(function () {
    // input validation
    if (isNaN(inputTextStep.text.valueOf())) {
        inputTextStep.text = "4";
    } else if (inputTextStep.text.valueOf() < 1) {
        inputTextStep.text = "1";
    } else if (inputTextStep.text.valueOf() > 999) {
        inputTextStep.text = "999";
    }
    updateTotalTimeCount();
});
newTaskChildGrid.addControl(inputTextStep, 1, 1);

var labelShortBreakMinute = new GUI.TextBlock(); // static label
labelShortBreakMinute.text = "Short Break:";
labelShortBreakMinute.color = "white";
labelShortBreakMinute.fontSizeInPixels = 24;
newTaskChildGrid.addControl(labelShortBreakMinute, 2, 0);

var inputShortBreakMinute = new GUI.InputText(); // textbox input for short break minute
inputShortBreakMinute.text = "5";
inputShortBreakMinute.fontSizeInPixels = 24;
inputShortBreakMinute.widthInPixels = 110;
inputShortBreakMinute.heightInPixels = 28;
inputShortBreakMinute.color = "white";
inputShortBreakMinute.onTextChangedObservable.add(function () {
    // input validation
    if (isNaN(inputShortBreakMinute.text.valueOf())) {
        inputShortBreakMinute.text = "5";
    } else if (inputShortBreakMinute.text.valueOf() < 1) {
        inputShortBreakMinute.text = "1";
    } else if (inputShortBreakMinute.text.valueOf() > 999) {
        inputShortBreakMinute.text = "999";
    }
    updateTotalTimeCount();
});
newTaskChildGrid.addControl(inputShortBreakMinute, 3, 0);

var labelLongBreakMinute = new GUI.TextBlock(); // static label
labelLongBreakMinute.text = "Long Break:";
labelLongBreakMinute.color = "white";
labelLongBreakMinute.fontSizeInPixels = 24;
newTaskChildGrid.addControl(labelLongBreakMinute, 2, 1);

var inputLongBreakMinute = new GUI.InputText(); // textbox input for long break minute
inputLongBreakMinute.text = "30";
inputLongBreakMinute.fontSizeInPixels = 24;
inputLongBreakMinute.widthInPixels = 110;
inputLongBreakMinute.heightInPixels = 28;
inputLongBreakMinute.color = "white";
inputLongBreakMinute.onTextChangedObservable.add(function () {
    // input validation
    if (isNaN(inputLongBreakMinute.text.valueOf())) {
        inputLongBreakMinute.text = "30";
    } else if (inputLongBreakMinute.text.valueOf() < 1) {
        inputLongBreakMinute.text = "1";
    } else if (inputLongBreakMinute.text.valueOf() > 999) {
        inputLongBreakMinute.text = "999";
    }
    updateTotalTimeCount();
});
newTaskChildGrid.addControl(inputLongBreakMinute, 3, 1);

var labelTotalTime = new GUI.TextBlock(); // static label
labelTotalTime.text = "Total: ";
labelTotalTime.color = "white";
labelTotalTime.fontSizeInPixels = 24;
labelTotalTime.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
newTaskChildGrid.addControl(labelTotalTime, 4, 0);

var labelTotalTimeCount = new GUI.TextBlock(); // static label used for total time of current added task
labelTotalTimeCount.text = "N/A";
labelTotalTimeCount.color = "white";
labelTotalTimeCount.fontSizeInPixels = 24;
labelTotalTimeCount.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
newTaskChildGrid.addControl(labelTotalTimeCount, 4, 1);

var updateTotalTimeCount = function () {
    // update labelTotalTimeCount label
    var totalTime = parseInt(inputTextMinute.text.valueOf()) 
    * parseInt(inputTextStep.text.valueOf()) 
    + parseInt(inputShortBreakMinute.text.valueOf()) 
    * (parseInt(inputTextStep.text.valueOf()) - 1)
    + parseInt(inputLongBreakMinute.text.valueOf());
    if (!isNaN(totalTime)) {
        // if totalTime is not NaN
        labelTotalTimeCount.text = totalTime.toString() + " Minutes";
    }
}
updateTotalTimeCount();

var addButton = new GUI.Button.CreateSimpleButton("addBtn", "Add to Queue"); // button for adding task to queue
addButton.width = "160px"
addButton.height = "50px";
addButton.color = "white";
addButton.paddingRightInPixels = 10;
addButton.paddingBottomInPixels = 10;
addButton.onPointerUpObservable.add(function () {
    // pass inputs from textboxes to TASK_MANAGER
    TASK_MANAGER.addTask(inputTextTask.text, parseInt(inputTextMinute.text.valueOf()), parseInt(inputTextStep.text.valueOf()), parseInt(inputShortBreakMinute.text.valueOf()), parseInt(inputLongBreakMinute.text.valueOf()));
    populateTaskList();
    inputTextTask.text = "Unnamed Task";
    newTaskScreen.isVisible = false; // hide current screen
});
newTaskChildGrid.addControl(addButton, 5, 0);

var cancelButton = new GUI.Button.CreateSimpleButton("cancelBtn", "Cancel");
cancelButton.width = "160px";
cancelButton.height = "50px";
cancelButton.color = "white";
cancelButton.paddingRightInPixels = 10;
cancelButton.paddingBottomInPixels = 10;
cancelButton.onPointerUpObservable.add(function () {
    newTaskScreen.isVisible = false; // hide current screen
});
newTaskChildGrid.addControl(cancelButton, 5, 1);
// end of new task screen

// begin of task list screen
export var taskListScreen = new GUI.Grid(); // screen for all tasks on a list
var taskListScreenRowCount = 0; // record row count for taskListScreen
taskListScreen.widthInPixels = 500;
taskListScreen.height = "95%"
taskListScreen.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
taskListScreen.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
taskListScreen.addColumnDefinition(1);

var populateTaskList = function () {
    // fill taskListScreen with all tasks from TASK_MANAGER
    clearTaskListScreen();
    var tasks = TASK_MANAGER.getTaskList();

    for (var index = 0; index < tasks.length; ++index) {
        taskListScreen.addRowDefinition(36, true);
        taskListScreenRowCount++;

        var taskNameLabel = new GUI.TextBlock();
        taskNameLabel.text = tasks[index].name;
        taskNameLabel.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
        taskNameLabel.paddingRightInPixels = 16;
        taskNameLabel.fontSizeInPixels = 24;
        taskNameLabel.color = "white";

        taskListScreen.addControl(taskNameLabel, index, 0);
    }
}

var clearTaskListScreen = function () {
    for (var index = taskListScreenRowCount - 1; index >= 0; --index) {
        taskListScreen.removeRowDefinition(index);
    }
    taskListScreenRowCount = 0;
}
// end of task list screen

// begin of status bar
export var statusBar = new GUI.Grid(); // status bar grid
statusBar.width = "100%";
statusBar.height = "5%";
statusBar.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;
statusBar.background = "grey";
statusBar.addColumnDefinition(0.6);
statusBar.addColumnDefinition(0.1);
statusBar.addColumnDefinition(0.1);
statusBar.addColumnDefinition(0.1);
statusBar.addColumnDefinition(0.1);

var currentExecuteLabel = new GUI.TextBlock(); // static label for current executing task
currentExecuteLabel.text = "Paused";
currentExecuteLabel.color = "white";
currentExecuteLabel.fontSizeInPixels = 24;
currentExecuteLabel.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
currentExecuteLabel.paddingLeftInPixels = 16;
statusBar.addControl(currentExecuteLabel, 0, 0);

var executeButton = new GUI.Button.CreateSimpleButton("exeBtn", "Next");
executeButton.color = "white";
executeButton.background = "#21610B";
executeButton.onPointerUpObservable.add(function () {
    TASK_MANAGER.startTask();
    populateTaskList();
});
statusBar.addControl(executeButton, 0, 1);
var pauseButton = new GUI.Button.CreateSimpleButton("pauseBtn", "Pause");
pauseButton.color = "white";
pauseButton.onPointerUpObservable.add(function () {
    TASK_MANAGER.pauseCurrentTask();
});
statusBar.addControl(pauseButton, 0, 2);
var queuePanelButton = new GUI.Button.CreateSimpleButton("qpBtn", "New Task");
queuePanelButton.color = "white";
queuePanelButton.onPointerUpObservable.add(function () {
    if (newTaskScreen.isVisible == true) {
        newTaskScreen.isVisible = false;
    } else {
        newTaskScreen.isVisible = true;
    }
    
});
statusBar.addControl(queuePanelButton, 0, 3);
var clearButton = new GUI.Button.CreateSimpleButton("clearBtn", "Remove All");
clearButton.color = "white";
clearButton.onPointerUpObservable.add(function () {
    TASK_MANAGER.clearTask();
    populateTaskList();
});
statusBar.addControl(clearButton, 0, 4);

export var updateStatusBar = function() {
    currentExecuteLabel.text = TASK_MANAGER.getCurrentTaskStatus(); // update status bar text, called every frame by main
}
// end of status bar