import * as BABYLON from 'babylonjs';
import * as GUI from "babylonjs-gui";

import path from 'path'

import * as UIELEMENT from 'common/ui'; // import UI elements from ui.js
import * as TASK_MANAGER from 'common/taskManager'; // import task manager for game loop

const isDevelopment = process.env.NODE_ENV !== 'production'

window.addEventListener('DOMContentLoaded', function () {
    // get the canvas DOM element
    var canvas = document.getElementById('renderCanvas');

    // load the 3D engine
    var engine = new BABYLON.Engine(canvas, true);

    var createScene = function () {
        var scene = new BABYLON.Scene(engine);
        var advancedTexture = new GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI"); // advanced texture for fullscreen UI
        var camera = new BABYLON.UniversalCamera("Camera", BABYLON.Vector3.Zero(), scene); // add an ArcRotateCamera

        var pathToAsset = path.join(__static, '/babylon/'); // use `path` to create the full path to our asset
        // change pathToAsset to relative path due to CORS policy when in development mode
        if (isDevelopment) {
            pathToAsset = "babylon/";
        }

        // append all elements from scene.babylon file
        BABYLON.SceneLoader.Append(pathToAsset, "scene.babylon", scene, function (newMeshes) {
            scene.activeCamera.attachControl(canvas, false);
            scene.activeCamera.fov = 0.8;
            window.oncontextmenu = function () {
                if (scene.activeCamera.fov == 0.8) {
                    scene.activeCamera.fov = 0.6;
                } else if (scene.activeCamera.fov == 0.6) {
                    scene.activeCamera.fov = 0.4;
                } else if (scene.activeCamera.fov == 0.4) {
                    scene.activeCamera.fov = 0.2;
                } else {
                    scene.activeCamera.fov = 0.8;
                }
            };
        });
        
        camera.inputs.removeByType("FreeCameraKeyboardMoveInput");

        advancedTexture.addControl(UIELEMENT.fpsLabel); // add fpsLabel to screen
        advancedTexture.addControl(UIELEMENT.newTaskScreen); // add newTaskScreen to screen
        advancedTexture.addControl(UIELEMENT.taskListScreen); // add taskListScreen to screen
        advancedTexture.addControl(UIELEMENT.statusBar); // add statusBar to screen

        return scene;
    }

    var scene = createScene(); // call the createScene function

    // run the render loop
    engine.runRenderLoop(function () {
        UIELEMENT.fpsLabel.text = "FPS: " + engine.getFps().toFixed(2); // get engine fps and round it to 2 decimals
        UIELEMENT.updateStatusBar(); // update status bar
        TASK_MANAGER.runLoop(engine.getDeltaTime());

        scene.render();
    });

    // the canvas/window resize event handler
    window.addEventListener('resize', function () {
        engine.resize();
    });

});